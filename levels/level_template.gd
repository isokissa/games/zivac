extends Node

class_name LevelTemplate

export var number_of_walls: int = 6
export var initial_balls: int = 10
export var initial_length: float = 0.9
export var initial_wall_speed: float = 200.0
export var initial_ball_velocity: float = 150.0

var high_score: int = 0

#Expecting the following nodes: 
#	Line: contains the points for wall lines
#	Layout: contains visual elements for the level. In future those elements 
#		can also contribute in score calculation. Within Layout, the 
#		position of the node named Spawner will be used as the starting position 
#		for the Ball

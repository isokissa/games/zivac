extends "res://addons/gut/test.gd"

var arena
var wall_generator


func before_all():
	arena = load("res://arena.tscn").instance()


func before_each():
	pass
#	arena.set_level("test")

class TestWallGenerator:
	extends 'res://addons/gut/test.gd'
	
	var test_points: PoolVector2Array
	var wall_generator
	const points = [
		Vector2(100, 50),
		Vector2(500, 50),
		Vector2(500, 350),
		Vector2(100, 350)
	]

	
	func before_all(): 
		test_points = PoolVector2Array(points)
		wall_generator = self.arena.get_node("Wall")
	
	
	func before_each():
		wall_generator.set_points(test_points)

	
	func test_initialize(): 
		assert_eq(1400.0, wall_generator.total_length)
		assert_eq([400.0, 700.0, 1100.0, 1400.0], wall_generator.segment_lengths)

		
	func test_find_point():		
		assert_eq(Vector2(100, 50), wall_generator.find_point(0))
		assert_eq(Vector2(101, 50), wall_generator.find_point(1))
		assert_eq(Vector2(101, 50), wall_generator.find_point(1401))
		assert_eq(Vector2(500, 50), wall_generator.find_point(400))
		assert_eq(Vector2(500, 51), wall_generator.find_point(401))
		assert_eq(Vector2(100, 51), wall_generator.find_point(1399))
		
		
	func test_create_wall_segment_single():
		assert_eq(PoolVector2Array([Vector2(100, 50), Vector2(101, 50)]),
				  wall_generator.create_wall_segment(0, 1))
		assert_eq(PoolVector2Array([Vector2(500, 50), Vector2(500, 51)]),
				  wall_generator.create_wall_segment(400, 401))
		assert_eq(PoolVector2Array([Vector2(105, 50), Vector2(205, 50)]),
				  wall_generator.create_wall_segment(5, 105))
		assert_eq(PoolVector2Array([Vector2(450, 50), Vector2(500, 50)]),
				  wall_generator.create_wall_segment(350, 400))
		assert_eq(PoolVector2Array([Vector2(450, 50), Vector2(500, 50)]),
				  wall_generator.create_wall_segment(350, 400))
		assert_eq(PoolVector2Array([Vector2(100, 350), Vector2(100, 51)]),
				  wall_generator.create_wall_segment(1100, 1399))
		assert_eq(PoolVector2Array([Vector2(100, 350), Vector2(100, 50)]),
				  wall_generator.create_wall_segment(1100, 1400))


	func test_create_wall_segment():
		assert_eq(PoolVector2Array([Vector2(100, 50), 
									Vector2(500, 50),
									Vector2(500, 51)]),
				  wall_generator.create_wall_segment(0, 401))
		assert_eq(PoolVector2Array([Vector2(101, 50), 
									Vector2(500, 50),
									Vector2(500, 51)]),
				  wall_generator.create_wall_segment(1, 401))
		assert_eq(PoolVector2Array([Vector2(500, 50), 
									Vector2(500, 350),
									Vector2(100, 350),
									Vector2(100, 249)]),
				  wall_generator.create_wall_segment(400, 1201))
		 

	func test_create_wall_segment_around_origin():
		assert_eq(PoolVector2Array([Vector2(110, 50), 
									Vector2(500, 50),
									Vector2(500, 350),
									Vector2(100, 350),
									Vector2(100, 50),
									Vector2(101, 50)]),
				  wall_generator.create_wall_segment(10, 1401))

		

extends Node

class_name WallGenerator
		
var points: PoolVector2Array
var segment_lengths: Array
var total_length 
var wall_segment_template
var wall_root_node
	

func set_points(points: PoolVector2Array): 
	self.points = points
	segment_lengths = calc_segment_lengths(points)
	total_length = segment_lengths[points.size() - 1]
	

func calc_segment_lengths(points: PoolVector2Array):
	var lengths = []
	var cumulative = 0
	var previous_point: Vector2 = Vector2.INF
	for point in points: 
		if previous_point != Vector2.INF:
			cumulative += previous_point.distance_to(point)
			lengths.append(cumulative)
		previous_point = point
	lengths.append(cumulative + previous_point.distance_to(points[0]))	
	return lengths

	
func find_segment_index(normalized_position: float) -> int:
	var index = 0
	while normalized_position > segment_lengths[index]: 
		index = index + 1
	return index


func normalize_position(position: float) -> float:
	var normalized_position = fmod(position, total_length)
	if normalized_position < 0: 
		normalized_position = total_length - abs(normalized_position)
	return normalized_position

	
func find_point(position: float) -> Vector2:
	var normalized_position = normalize_position(position)
	var index = find_segment_index(normalized_position)
	var start_point = points[index]
	var end_point = points[(index + 1) % points.size()]
	var segment_len = segment_lengths[index]
	if index > 0: 
		segment_len = segment_len - segment_lengths[index - 1]
		normalized_position = normalized_position - segment_lengths[index - 1]
	return start_point.linear_interpolate(end_point, 
										  normalized_position / segment_len)


func _append_if_different(a: Array, new_element: float) -> void: 
	var normalized_element = normalize_position(new_element) 
	if abs(a[-1] - normalized_element) > 0.1:
		a.append(normalized_element)
	
	
func create_wall_segment(start_pos: float, end_pos: float) -> PoolVector2Array: 
	assert(start_pos < end_pos)
	var norm_start_pos = normalize_position(start_pos)
	var norm_end_pos = normalize_position(end_pos)
	if abs(norm_start_pos - norm_end_pos) < 0.1: 
		return points
	var result_positions = [norm_start_pos]
	var current_index = find_segment_index(norm_start_pos)
	if norm_start_pos >= norm_end_pos:
		while current_index < self.points.size():
			_append_if_different(result_positions, segment_lengths[current_index])
			current_index = current_index + 1
		current_index = 0
	while self.segment_lengths[current_index] < norm_end_pos:
		_append_if_different(result_positions, segment_lengths[current_index])
		current_index = current_index + 1
	_append_if_different(result_positions, norm_end_pos)
	var result_points = []
	for pos in result_positions: 
		result_points.append(find_point(pos))
	return PoolVector2Array(result_points)


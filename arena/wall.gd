extends StaticBody2D

class_name Wall

func set_points(points: PoolVector2Array):
	get_node("DisplayLine").points = points
	var collision_polygon: PoolVector2Array = PoolVector2Array(points)
	var returning_polygon: PoolVector2Array = collision_polygon
	returning_polygon.invert()
	returning_polygon.remove(0)
	collision_polygon.append_array(returning_polygon)
	get_node("CollisionPolygon2D").set_polygon(collision_polygon)

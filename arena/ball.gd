extends KinematicBody2D


var linear_velocity: Vector2
var wall_direction: float


func _physics_process(delta):
	var collision = move_and_collide(linear_velocity * delta)
	if collision: 
		linear_velocity = linear_velocity.bounce(collision.normal)
		linear_velocity = linear_velocity.rotated(wall_direction * 0.6)
		$BallPing.stop()
		$BallPing.play()

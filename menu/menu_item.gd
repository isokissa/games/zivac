extends Button

class_name MenuItem

export var level_name: String = "Human-readable level name"
var level_id: String


func _ready():
	text = level_name
	level_id = name  # use the name of the button as ID


func _on_StartLevelButton_pressed():
	Global.goto_arena(level_id)

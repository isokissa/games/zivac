extends Node

onready var arena = preload("res://arena/arena.tscn").instance()

onready var current_scene = get_tree().get_root().get_node("Menu")

var current_level: String = ""
var high_scores = {}


func set_level(level_name: String):
	if !high_scores.has(level_name):
		high_scores[level_name] = 0
	current_level = level_name


func set_scene(scene: String):
	if current_scene != null: 
		current_scene.queue_free()
	var s = ResourceLoader.load(scene)
	current_scene = s.instance()
	get_tree().get_root().add_child(current_scene)


func goto_menu(): 
	set_scene("res://menu/menu.tscn")	


func goto_arena(level: String):
	current_level = level
	set_scene("res://arena/arena.tscn")
